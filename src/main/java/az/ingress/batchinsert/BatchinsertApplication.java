package az.ingress.batchinsert;

import az.ingress.batchinsert.model.Account;
import az.ingress.batchinsert.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class BatchinsertApplication implements CommandLineRunner {
private final AccountService accountService;
    public static void main(String[] args) {
        SpringApplication.run(BatchinsertApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        List<Account> accounts = createAccounts();
       accountService.saveAllInBatch(accounts);
      //  accounts.forEach(account -> System.out.println(account));
    }

    private List<Account> createAccounts() {
        List<Account> accounts = new ArrayList<>();
        for (int i = 1; i <= 1000; i++) {
            accounts.add(Account.builder().name("Huseyn"+i).balance(100).build());
        }
        return accounts;
    }
}
