package az.ingress.batchinsert.service;

import az.ingress.batchinsert.model.Account;
import az.ingress.batchinsert.repository.AccountRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService{
    private final AccountRepository accountRepository;
    @Override
    @Transactional
    public void saveAllInBatch(List<Account> accountList) {
        accountRepository.saveAll(accountList);
    }
}
