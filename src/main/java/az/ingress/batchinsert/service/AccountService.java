package az.ingress.batchinsert.service;

import az.ingress.batchinsert.model.Account;

import java.util.List;

public interface AccountService {
     void saveAllInBatch(List<Account> accountList);
}
