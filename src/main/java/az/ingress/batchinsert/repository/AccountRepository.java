package az.ingress.batchinsert.repository;

import az.ingress.batchinsert.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account,Integer> {
}
